$('#reservasModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var hotel = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('Registrar reserva para el Hotel ' + hotel)
  })

  $(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:2000});

    $('#reservasModal').on('show.bs.modal', function (e){
      console.log('El modal de reserva se está mostrando');
      //Obtengo el boton que llamo al modal y le cambio la clase
      var button = $(e.relatedTarget) // Button that triggered the modal
      button.removeClass('btn-success');
      button.addClass('btn-primary');
      button.prop('disabled', true);
    });
    $('#reservasModal').on('shown.bs.modal', function (e){
      console.log('El modal de reserva se mostró');
    });

    $('#reservasModal').on('hide.bs.modal', function (e){
      console.log('El modal de reserva se está ocultando');
    });
    $('#reservasModal').on('hidden.bs.modal', function (e){
      console.log('El modal de reserva se ocultó');
      //Cuando se oculta el modal vuelvo a activar todos los botones que tengan data-target al modal reservasModal
      //y restablezco los estilos de los botones
      $('button[data-target="#reservasModal"]').removeClass('btn-primary');
      $('button[data-target="#reservasModal"]').addClass('btn-success');
      $('button[data-target="#reservasModal"]').prop('disabled', false);
    });
  });